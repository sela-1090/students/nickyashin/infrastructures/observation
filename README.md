
# install Prometheus & Grafana

# Add the repositories
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo add grafana https://grafana.github.io/helm-charts
helm repo update

# Install Prometheus
helm install prometheus prometheus-community/prometheus

# Expose the prometheus-server service to the internet using nodeport
kubectl expose service prometheus-server — type=NodePort — target-port=9090 — name=prometheus-server-ext

# run prometheus
minikube service prometheus-server-ext

# Install Grafana
helm install grafana grafana/grafana

# Expose Grafana service
kubectl expose service grafana — type=NodePort — target-port=3000 — name=grafana-ext

# run grafana
minikube service grafana-ext

# log in: 
http://127.0.0.1:60200/

# to uninstall both run the commands: 
helm uninstall prometheus
helm uninstall grafana
